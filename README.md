
# Validated Network Demo

## Overview
This demo uses the network.base validated collection to automat a brownfield multi-vendor network environmwent.

## Prereq Setup 
This Validated demo was developed to work using the RHDP Network Workshop lab environment. This is the same lab environment with 4 multi-vendor routers used for the Ansible Network Automation Workshop for beginners. Red Hatters can access here: https://demo.redhat.com/catalog?search=network&item=babylon-catalog-prod%2Fansiblebu.aap2-workshop-networking-automation.prod

**Non- Red Hatters** You can request access to a Red Hat Demo Platform (RHDP) from your Red Hat account team or feel free to modify inventories etc to work with your own lab environment.

- Please note, you will need an Execution Environment with the Validated Content `network.base' installed. The path to the resource_manager role should be defined in the ansible.cfg file.  

**Gitlab** You will need a Gitlab account to fork this repo to your own account.

**Token** You will need to go to settings in Gitlab ans create a project access token for your forked project with the maintainer role and privileges to read and write to the repo. The default is Guest with no privileges. Also set a suitable end date for your Token.


## Demo Setup
Your lab environment link with credential will look somehting like this:
https://pw54v.example.opentlc.com/#studentinfo

### Step 1:
Clone down your forked repo to your VSCode running in the RHPD student POD

```
git clone https://gitlab.com/your_username/validated_network_demo.git
```

### Step 2:
1. Open the file explorer to your validated_network_demo/ folder.
2. Open a new terminal (ensure the terminal is also in the validated_network/ folder)

### Step 3
Add your token to Lab VSCode at the terminal. This avoids adding username and password each change to Git.
```
git remote set-url origin https://username:token@gitlab.com/username/validated_network_demo.git
```
### Step 4
The setup.yml playbook will configure the Ansible Controler as code for the necessary controller constructs used in the demo. 


Run the `ansible-navigator` command with the `run` argument and -m stdout as well as `extra-vars` -e 

```bash
$ ansible-navigator run setup.yml -m stdout -e "username=your-gitlab-user token=your-gitlab-token password=your-lab-student-password" -v
```

## Running the Demo

### Step 1
Make sure there is no a host_vars directory. If so  `rm -rf host_vars` and update git.

**updating GIT**
Use the builtin Git integration with VSCOde or the folowing git commands:
```
git add --all
git commit -m 'changed'
git push 
```

### Step 2 
Run the Network-Validated-Persist job template in the AAP Controller in your lab.

### Step 3 
Check your forked Gitlab repo for the host_vars

### Step 4
1. Add the network prefix 192.168.3.3 to rtr1 in host_vars/bgp_address_family.yaml
2. Add the network prefix 192.168.4.4 to rtr2 in host_vars/bgp_address_family.yaml

### Step 5 
Run the Network-Validated-Deploy job-template in the AAP Controller in your lab.

- Deploy uses the state of merged to add new configurations

### Step 6
Ooops - Add a incorect network prefix to rtr 2 using OOB CLI.
- see oops.txt

```
ssh rtr2

conf t
router bgp 65001
  address-family ipv4
  network 192.168.5.5/32
exit
end
```
### Step 7
Run the Network-Validated-Detect job-template in the AAP Controller in your lab.

- Detect will identify any Configuration drift between your Single Source of Truth (SSOT) `host_vars` and the router's running configuration.

### Step 8
Run the Network-Validated-Remediate job-template in the AAP Controller in your lab.

- Remediate will overwrite any extra configuration or add any missing configuration from the SSOT.